export enum FunctionComponentType {
	STANDARD = 0,
	COMPLEX = 1,
	BUILDING = 2
}

interface IConnectorConfig {
	fill: string;
	stroke: string;
}
interface IConfigComponent {
	size: number;
	fillColor: string;
	strokeColor: string;
	strokeWidth: number;
	connectors: {
		width: number,
		height: number,
		inputs: IConnectorConfig,
		outputs: IConnectorConfig
	};

}

export interface IFunctionComponentConfig {
	title: string;
	image: string;
	id: number;
	type: FunctionComponentType;
}
interface IZoomConfig {
	mouseZoomSpeed: number;
	zoomRangeShowSteps: boolean;
	zoomRangeSize: {
		min: number,
		max: number
	};
	zoomRangeStepIndicator: number[];
}
interface IConfigType {
	warnOnLeave: boolean;
	functionComponent: IConfigComponent;
	fcAmount: {
		STANDARD: number,
		COMPLEX: number,
		BUILDING: number
	};
	fcTypeConnectors: {
		STANDARD: [number, number],
		COMPLEX: [number, number],
		BUILDING: [number, number]
	};
	booleanComponent: IConfigComponent;
	ioComponent: IConfigComponent;
	merkerComponent: IConfigComponent;
	fcTypes: IFunctionComponentConfig[];
	fcImagePrefix: string;
	zoomConfig: IZoomConfig;
}

/*  Configuration File
*
*   This file contains the whole configuration for the website
*   and needs to be compiled too.
*/
const config: IConfigType = {
	////////////////////////////////////////
	//          General Settings          //            
	////////////////////////////////////////
	// When enabled, there will be a prompt before leaving,
	// so the user doesn't loose the current progress
	warnOnLeave: false,
	fcImagePrefix: "./resources/images/functionComponents",
	functionComponent: {
		size: 250,
		fillColor: "white",
		strokeColor: "#565656",
		strokeWidth: 2,
		connectors: {
			height: 25,
			width: 40,
			inputs: {
				fill: "blue",
				stroke: "black"
			},
			outputs: {
				fill: "red",
				stroke: "black"
			}
		}
	},
	fcAmount: {
		STANDARD: 100,
		COMPLEX: 100,
		BUILDING: 10
	},
	fcTypeConnectors: {
		STANDARD: [3, 2],
		COMPLEX: [5, 3],
		BUILDING: [7, 3]
	},
	fcTypes: [
		{
			title: "No Logic",
			image: "0.svg",
			id: 0,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "No Logic",
			image: "0.svg",
			id: 0,
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "No Logic",
			image: "0.svg",
			id: 0,
			type: FunctionComponentType.BUILDING
		},
		{
			title: "Buffer",
			image: "1.svg",
			id: 1,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "NOT",
			image: "2.svg",
			id: 2,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Rising Trigger",
			image: "3.svg",
			id: 3,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Falling Trigger",
			image: "4.svg",
			id: 4,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "DCF77",
			image: "5.svg",
			id: 5,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "AND",
			image: "16.svg",
			id: 16,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "OR",
			image: "17.svg",
			id: 17,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "XOR",
			image: "18.svg",
			id: 18,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Lower than",
			image: "19.svg",
			id: 19,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Greater than",
			image: "20.svg",
			id: 20,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "RS",
			image: "21.svg",
			id: 21,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "SR",
			image: "22.svg",
			id: 22,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Addition",
			image: "24.svg",
			id: 24,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Subtraction",
			image: "25.svg",
			id: 25,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Multiplication",
			image: "26.svg",
			id: 26,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Division",
			image: "27.svg",
			id: 27,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Minimum",
			image: "28.svg",
			id: 28,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Maximum",
			image: "29.svg",
			id: 29,
			type: FunctionComponentType.STANDARD
		},
		{
			title: "TOF",
			id: 32,
			image: "32.svg",
			type: FunctionComponentType.STANDARD
		},
		{
			title: "TON",
			id: 33,
			image: "33.svg",
			type: FunctionComponentType.STANDARD
		},
		{
			title: "TP",
			id: 34,
			image: "34.svg",
			type: FunctionComponentType.STANDARD
		},
		{
			title: "Multiplexer",
			id: 64,
			image: "64.svg",
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "Demultiplexer",
			id: 65,
			image: "65.svg",
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "Scale",
			id: 66,
			image: "66.svg",
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "Counter",
			id: 67,
			image: "67.svg",
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "PI-Regler",
			id: 68,
			image: "68.svg",
			type: FunctionComponentType.COMPLEX
		},
		{
			title: "Weekly time switch",
			id: 128,
			image: "128.svg",
			type: FunctionComponentType.BUILDING
		},
		{
			title: "Fan coil",
			id: 129,
			image: "129.svg",
			type: FunctionComponentType.BUILDING
		},
		{
			title: "Dimmer",
			id: 130,
			image: "130.svg",
			type: FunctionComponentType.BUILDING
		},
		{
			title: "Blind",
			id: 131,
			image: "131.svg",
			type: FunctionComponentType.BUILDING
		},
		{
			title: "Sun protection",
			id: 132,
			image: "132.svg",
			type: FunctionComponentType.BUILDING
		}
	],
	ioComponent: {
		size: 150,
		fillColor: "white",
		strokeColor: "#565656",
		strokeWidth: 2,
		connectors: {
			height: 25,
			width: 40,
			inputs: {
				fill: "grey",
				stroke: "black"
			},
			outputs: {
				fill: "grey",
				stroke: "black"
			}
		}
	},
	booleanComponent: {
		size: 150,
		fillColor: "white",
		strokeColor: "#565656",
		strokeWidth: 2,
		connectors: {
			height: 25,
			width: 40,
			inputs: {
				fill: "purple",
				stroke: "black"
			},
			outputs: {
				fill: "purple",
				stroke: "black"
			}
		}
	},
	merkerComponent: {
		size: 150,
		fillColor: "white",
		strokeColor: "#565656",
		strokeWidth: 2,
		connectors: {
			height: 25,
			width: 40,
			inputs: {
				fill: "green",
				stroke: "black"
			},
			outputs: {
				fill: "green",
				stroke: "black"
			}
		}
	},
	////////////////////////////////////////
	//         Zooming settings           //          
	////////////////////////////////////////
	zoomConfig: {
		// This number sets the speed of zooming via the scrollwheel,
		// as well as zooming via context menu
		// The higher this number, the faster the zoom will be
		mouseZoomSpeed: 2,
		// If set to true, little Merkers will be next to the zoom-range slider
		zoomRangeShowSteps: true,
		// The minimum and maximum zoom level
		// 100   -> no zoom
		// > 100 -> Zoomed in
		// < 100 -> Zoomed out
		zoomRangeSize: {
			min: 25,
			max: 400
		},
		// If zoomRangeShowSteps is set to true, these are the values
		// that are marked by a small grey line next to it
		zoomRangeStepIndicator: [
			50, 100, 150, 200, 250, 300, 350, 400
		],
	}
};

export { config as Config };