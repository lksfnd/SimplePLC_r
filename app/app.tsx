import { h, render } from 'preact';
import { SvgEditor, Header } from './components';
import { BoolComponent, IOComponent, MerkerComponent, FunctionComponent } from "./components/elements";
import { Config } from "./config";
import { WsHelper, ConnectionHelper, FCAddressHelper } from "./helper";

const renderApp = () => {
    render((
        <div height="100%">
            <Header title="Preact minimal 🚀" />
            <SvgEditor>
                <FunctionComponent x={350} id={0}></FunctionComponent>
                <FunctionComponent id={0} type={1}></FunctionComponent>
                <BoolComponent></BoolComponent>
                <IOComponent addr={1}></IOComponent>
                <MerkerComponent></MerkerComponent>
            </SvgEditor>
        </div>
    ), document.getElementById('root'));
};
window.onload = renderApp;
window["wshelper"] = new WsHelper("ws://127.0.0.1:8181");
window["build"] = ConnectionHelper;
window["fcHelper"] = FCAddressHelper;
//window.setInterval(ConnectionHelper.buildConnections, 1000);

if (process.env.NODE_ENV === 'production') {
    const runtime = require('offline-plugin/runtime');

    runtime.install({
        onUpdateReady: () => {
            // Tells to new SW to take control immediately
            runtime.applyUpdate();
        },
        onUpdated: () => {
            // Reload the webpage to load into the new version
            window.location.reload();
        },
    });
}