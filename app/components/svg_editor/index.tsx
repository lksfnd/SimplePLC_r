import { h, Component } from 'preact';
import bind from "bind-decorator";

const styles = require("./svg_editor.pcss");

export class SvgEditor extends Component<{}, {}> {
    public isMoving: boolean = false;
    public moveingElement: any;
    public first: [number, number];
    public moving_id: string;
    public reorderd: boolean = false;
    private viewBox: number[];
    private svg: Element;
    constructor(props) {
        super(props); 
    }

    private updateViewBox(x: number, y: number){
        this.viewBox = [-x,-y, this.svg.clientWidth, this.svg.clientHeight];
        this.svg.setAttribute("viewBox", this.viewBox.join(" "));
    }

    componentDidMount(){
        this.updateViewBox(0, 0);
    }

    @bind
    handleMouseDown(e: MouseEvent){
        if(e.target == this.svg){
            this.moveingElement = this;
            this.isMoving = true;
            this.first = [e.screenX + this.viewBox[0], e.screenY + this.viewBox[1]];
        }
    }
    
    @bind
    handleMove(x: number, y: number){
        this.updateViewBox(x,y);
    }

    @bind
    handleMouseMove(e: MouseEvent){
        if(this.isMoving){
            if(this.reorderd == false){
                this.reorderd = true;
                let ele = this.svg.querySelector(`g[data-id="${this.moving_id}"]`);
                this.svg.removeChild(ele);
                this.svg.appendChild(ele);
            }
            let x = e.screenX - this.first[0];
            let y = e.screenY - this.first[1];
            if(this.moveingElement == this){
                this.svg.setAttribute("style",`background-position: ${x} ${y}`);
            }
            this.moveingElement.handleMove(x,y);
        }
    }

    @bind
    handleMouseUp(){
        this.reorderd = false;
        this.isMoving = false;
        this.moveingElement = undefined;
        this.first = undefined;
    }

    render() {
        return (
            <svg
                ref={(s)=>this.svg = s}
                onMouseDown={this.handleMouseDown}
                onMouseMove={this.handleMouseMove}
                onMouseUp={this.handleMouseUp}
                className={styles['svg']}
                preserveAspectRatio="xMinYMin slice"
                xmlns="http://www.w3.org/2000/svg">
                {this.props.children.map((v)=> {
                    v.attributes = v.attributes || {};
                    v.attributes.parent = this;
                    return v
                })}
            </svg>
        );
    }
}
