import { h } from 'preact';

const styles = require('./header.pcss');
export function Header(props) {
    return (<header className={styles['header']}>
        <h1>{props.title}</h1>
    </header>);
}