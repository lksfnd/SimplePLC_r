import { h, Component } from 'preact';
import { Connector, Draggable } from "../../helper";
import { Config } from "../../../config";
import { IElement } from "../element";

// PCSS import
const styles = require('./MerkerComponent.pcss');

/**
 * The merker component that has a clickable output to connect.
 */
@Draggable
export class MerkerComponent extends Component<any, {
    addr: number
}> implements IElement {
    input: Connector;
    output: Connector;

    getConnInfo(e: Connector) {
        return { bySourceType: 5, byChannel: this.state.addr }
    }

    /**
     * Creates a new bool component.  
     * The current value to be displayed is initialized by props.value
     * 
     * @param props Properties
     */
    constructor(props) {

        super(props);

        // Initial value ('-') displayed under the title in green
        this.state.addr = props.addr || 0;

    }
    render() {

        return (
            <g>
                <rect x="0" y="0" width="75" height="75" fill="white" stroke="black" />
                <Connector
                    ref={(e) => this.input = e}
                    parent={this}
                    x={-Config.merkerComponent.connectors.width}
                    y={25}
                    width={Config.merkerComponent.connectors.width}
                    height={Config.merkerComponent.connectors.height}
                    fill={Config.merkerComponent.connectors.inputs.fill}
                    stroke={Config.merkerComponent.connectors.inputs.stroke}
                    stroke-width="1"
                    type="input" />
                <Connector
                    ref={(e) => this.output = e}
                    parent={this}
                    x={75}
                    y={25}
                    width={Config.merkerComponent.connectors.width}
                    height={Config.merkerComponent.connectors.height}
                    fill={Config.merkerComponent.connectors.inputs.fill}
                    stroke={Config.merkerComponent.connectors.inputs.stroke}
                    stroke-width="1"
                    type="output" />
                <text text-anchor="middle" x="37" y="25" className={styles['title']}>
                    Merker
                </text>

                <text text-anchor="middle" x="37" y="55" className={styles['value']} fill="#33cc33">
                    {this.state.addr}
                </text>
            </g>
        );
    }
}