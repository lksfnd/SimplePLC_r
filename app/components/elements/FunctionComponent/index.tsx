import { h, Component, cloneElement, } from 'preact';
import { Connector, Draggable } from '../../helper';
import { FunctionComponentType, IFunctionComponentConfig, Config } from "../../../config";
import { IElement } from "../element";
import { ConnectionHelper, FCAddressHelper } from "../../../helper";

@Draggable
export class FunctionComponent extends Component<any, {
		inputs: Array<Connector>,
		outputs: Array<Connector>,
		x: number,
		y: number,
		inputOffset: number,
		outputOffset: number,
		id: number,
		image: string,
	}> implements IElement {

	getConnInfo(e: Connector) {
		let i = this.outputConnectors.indexOf(e);

		return { bySourceType: 8 + this.props.type + (i + 1) << 4, byChannel: this.addr + 1 }
	}

	inputConnectors: Array<Connector> = [];
	outputConnectors: Array<Connector> = [];
	addr: number;
	type: FunctionComponentType;
	static getInputsOutputs(type: FunctionComponentType): [number, number] {
		switch (type) {
			case FunctionComponentType.STANDARD:
				return Config.fcTypeConnectors.STANDARD;
			case FunctionComponentType.COMPLEX:
				return Config.fcTypeConnectors.COMPLEX;
			case FunctionComponentType.BUILDING:
				return Config.fcTypeConnectors.BUILDING;
			default:
				return Config.fcTypeConnectors.STANDARD;
		}
	}

	componentWillUnmount() {
		FCAddressHelper.freeAddress(this);
	}

	constructor(props) {
		super(props);
		this.state.x = props.x || 0;
		this.state.y = props.y || 0;
		this.state.id = props.id || 0;
		let fcConfig: IFunctionComponentConfig;
		if (this.state.id === 0 && props.type) {
			fcConfig = Config.fcTypes.find((e) => e.id === 0 && e.type === props.type);
			this.type = props.type;
			this.state.image = Config.fcImagePrefix + "/0.svg";
		} else {
			fcConfig = Config.fcTypes.find((e) => e.id === this.state.id);
			this.type = fcConfig.type;
			this.state.image = Config.fcImagePrefix + "/" + fcConfig.image;
		}
		this.addr = FCAddressHelper.getAddress(this);
		const [inputNumber, outputNumber] = FunctionComponent.getInputsOutputs(fcConfig.type);
		const inputOffset = (this.state.y + Config.functionComponent.size) / (2 * inputNumber);
		const outputOffset = (this.state.y + Config.functionComponent.size) / (2 * outputNumber);
		const inputOffsetOffset = Math.abs(inputOffset - Config.functionComponent.connectors.height);
		const outputOffsetOffset = Math.abs(outputOffset - Config.functionComponent.connectors.height);
		this.state.inputOffset = inputOffset;
		const inputs = [], outputs = [];
		for (let i = 0; i < inputNumber; i++) {
			inputs.push(<Connector
				ref={(e) => { this.inputConnectors[i] = e }}
				parent={this}
				type="input"
				x={this.state.x}
				y={i * 2 * inputOffset + inputOffset / 2 + inputOffsetOffset / 2}
				width={Config.functionComponent.connectors.width}
				height={Config.functionComponent.connectors.height}
				fill={Config.functionComponent.connectors.inputs.fill}
				stroke={Config.functionComponent.connectors.inputs.stroke}
				stroke-width="1"
			></Connector>);
		}

		this.state.inputs = inputs;
		for (let i = 0; i < outputNumber; i++) {
			outputs.push(
				<Connector
					ref={(e) => { this.outputConnectors[i] = e }}
					parent={this}
					type="output"
					x={this.state.x + Config.functionComponent.connectors.width + Config.functionComponent.size}
					y={i * 2 * outputOffset + outputOffset / 2 + outputOffsetOffset / 2}
					width={Config.functionComponent.connectors.width}
					height={Config.functionComponent.connectors.height}
					fill={Config.functionComponent.connectors.outputs.fill}
					stroke={Config.functionComponent.connectors.outputs.stroke}
					stroke-width="1"
				></Connector >
			);
		}
		this.state.outputs = outputs;
		ConnectionHelper.addFunctionComponent(this);
	}
	shouldComponentUpdate() {
		return false;
	}
	render() {
		return <g>
			<g>{this.state.inputs}</g>
			<g>
				<rect
					x={this.state.x + Config.functionComponent.connectors.width}
					y={this.state.y}
					height={Config.functionComponent.size}
					width={Config.functionComponent.size}
					fill={Config.functionComponent.fillColor}
					stroke={Config.functionComponent.strokeColor}
				/>
				<image
					x={this.state.x + Config.functionComponent.connectors.width}
					y={this.state.y}
					height={Config.functionComponent.size}
					width={Config.functionComponent.size}
					xlinkHref={this.state.image}
				/>
			</g>
			<g>{this.state.outputs}</g>
		</g >;
	}
}