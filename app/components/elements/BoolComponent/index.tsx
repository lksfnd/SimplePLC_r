import { h, Component } from 'preact';
import { Connector, Draggable } from "../../helper";
import { Config } from "../../../config";
import { IElement } from "../element";

// PCSS import
const styles = require('./BoolComponent.pcss');

/**
 * The boolean component that has a clickable output to connect.
 */
@Draggable
export class BoolComponent extends Component<any, {value: boolean}> implements IElement {

    /**
     * Creates a new bool component.  
     * The current value to be displayed is initialized by props.value
     * 
     * @param props Properties
     */
    constructor(props) {

        super(props);

        // Initial value ('-') displayed under the title in green
        this.state.value = props.value || false;
    }
    getConnInfo(e: Connector) {
        return { bySourceType: 6 + ((this.state.value) ? 0 : 1), byChannel: 0 }
    }
    render() {

        return (
            <g>
                <rect x="0" y="0" width="75" height="75" fill="white" stroke="black" />
                <Connector
                    parent={this}
                    x={75}
                    y={25}
                    width={Config.booleanComponent.connectors.width}
                    height={Config.booleanComponent.connectors.height}
                    fill={Config.booleanComponent.connectors.inputs.fill}
                    stroke={Config.booleanComponent.connectors.inputs.stroke}
                    stroke-width="1"
                    type="output" />
                <text text-anchor="middle" x="37" y="25" className={styles['title']}>
                    BOOL
                </text>

                <text text-anchor="middle" x="37" y="55" className={styles['value']} fill="#33cc33">
                    {this.state.value ? "TRUE" : "FALSE"}
                </text>
            </g>
        );
    }
}