import { Connector } from '../helper'
export interface IElement {
	getConnInfo(c: Connector): { bySourceType: number, byChannel: number }
}