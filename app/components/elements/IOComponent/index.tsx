import { h, Component } from 'preact';
import { Connector, Draggable } from "../../helper";
import { Config } from "../../../config";
import { IElement } from "../element";

// PCSS import
const styles = require('./IOComponent.pcss');

/**
 * The boolean component that has a clickable output to connect.
 */
@Draggable
export class IOComponent extends Component<any, {
    isInput: boolean,
    isDigital: boolean,
    addr: number
}> implements IElement {
    conn: Connector;

    getConnInfo(e: Connector) {
        return { bySourceType: 1 + ((this.state.isDigital) ? 0 : 1), byChannel: this.state.addr }
    }

    /**
     * Creates a new bool component.  
     * The current value to be displayed is initialized by props.value
     * 
     * @param props Properties
     */
    constructor(props) {

        super(props);

        // Initial value ('-') displayed under the title in green
        this.state.isInput = props.isInput || false;
        this.state.isDigital = props.isDigital || true;
        this.state.addr = props.addr || 0;

        this.toggleIO = this.toggleIO.bind(this);
        this.toggleDA = this.toggleDA.bind(this);
    }
    toggleIO(e: Event) {
        e.preventDefault();
        this.setState((o) => {
            o.isInput = !o.isInput;
        });
        this.conn.removeConnection();
    }
    toggleDA(e: Event) {
        e.preventDefault();
        this.setState((o) => {
            o.isDigital = !o.isDigital;
        });
        this.conn.removeConnection();
    }

    render() {
        return (
            <g onContextMenu={this.toggleIO} onDblClick={this.toggleDA}>
                <rect x="0" y="0" width="75" height="75" fill="white" stroke="black" />
                <Connector
                    ref={(e) => this.conn = e}
                    parent={this}
                    x={(this.state.isInput ? 75 : -Config.ioComponent.connectors.width)}
                    y={25}
                    width={Config.ioComponent.connectors.width}
                    height={Config.ioComponent.connectors.height}
                    fill={Config.ioComponent.connectors.inputs.fill}
                    stroke={Config.ioComponent.connectors.inputs.stroke}
                    stroke-width="1"
                    type={this.state.isInput ? "output" : "input"} />
                <text text-anchor="middle" x="37" y="25" className={styles['title']}>
                    IO
                </text>

                <text text-anchor="middle" x="37" y="55" className={styles['value']} fill="#33cc33">
                    {(this.state.isDigital ? "D" : "A") + (this.state.isInput ? "I" : "O") + this.state.addr}
                </text>
            </g>
        );
    }
}