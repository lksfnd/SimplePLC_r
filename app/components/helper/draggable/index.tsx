import { h } from 'preact';
import { SvgEditor } from "../../svg_editor"
import bind from "bind-decorator";

function Draggable<T extends {new(...args:any[]):any}>(constructor:T) {
	class DraggableZ extends constructor {
		private parent : SvgEditor;
		private translate_x: number = 0;
		private translate_y: number = 0;
		private data_id: string = "";
		private g: Element;
		constructor(...args: any[]){
			super(...args);
			this.parent = args[0].parent;
			this.state.data_id = "";
		}
		
		@bind
		handleOnMouseDown(e: MouseEvent){
			let id = Date.now().toString(); 
			this.parent.isMoving = true;
			this.parent.moveingElement = this;
			this.parent.first = [e.screenX, e.screenY];
			this.data_id = id;
			this.parent.moving_id = id;
			let transform = this.g.getAttribute("transform");
			if(transform != null){
				[this.translate_x, this.translate_y] = transform.replace(/translate\((.*),(.*)\)/, "$1 $2").split(" ").map(parseFloat);
			}
			this.forceUpdate();
		}

		@bind
		public handleMove(x: number, y: number){
			this.g.setAttribute("transform", `translate(${this.translate_x + x},${this.translate_y + y})`);
		}

		render(){
			return <g data-id={this.data_id} ref={(g)=>this.g = g} class="Draggable" onMouseDown={this.handleOnMouseDown}>{super.render()}</g>
		}
	}
	return DraggableZ
}

export {Draggable}
export default Draggable