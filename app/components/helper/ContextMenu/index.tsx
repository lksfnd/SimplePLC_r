import { h, Component } from 'preact';

const styles = require('./ContextMenu.pcss');

/**
 * The contextmenu element is displayed upon
 * rightclicking anywhere on the editor
 */
export class ContextMenu extends Component<any, any> {

    /**
     * Creates a new ContextMenu with the following parameters  
     *  * active `boolean`: True means the context menu is currently active/in use
     *  * options `array`: The options that are displayed once the menu is displayed
     *  * scope `HTMLElement`:
     *      The scope for the contextmenu to open,
     *      can be null, if the context menu needs to be attached manually
     * 
     * @param props Properties
     */
    constructor(props) {

        super(props);

        if (props.scope !== null) {
            if (props.scope instanceof HTMLElement) {

                let currentObject = this;

                props.scope.oncontextmenu = (event) => {

                    event.preventDefault();

                    let clickLocation = {
                        x: event.clientX,
                        y: event.clientY
                    };

                    currentObject.setState((s) => s.location = clickLocation);
                    currentObject.show();

                    return false;
                };
            }
        }

        // TODO: Replace options with 'real' options
        this.state = {
            active: props.active || false,
            options: props.options || [
                {
                    title: "Abort...",
                    onClick: () => { } // Abort does nothing --> menu hides
                }
            ],
            location: { x: 0, y: 0 }
        };
    }

    /**
     * Changes the currently active options that are displayed.
     * @param options (Array) the options that are displayed
     */
    setOptions(options) {

        this.setState((s) => s.options = options);

    }

    /**
     * Shows the context menu
     */
    show() {

        this.setState((s) => s.active = true);

    }

    /**
     * Hides the context menu
     */
    hide() {

        this.setState((s) => s.active = false);

    }

    /**
     * Returns the style options (internal)
     */
    getStyle() {

        // If the context menu is currently in use
        if (this.state.active) {
            // Returns the style-tag content for the menu when its displayed
            return "visibility:visible;display:block;top:"
                + this.state.location.y + "px;left:"
                + this.state.location.x + "px;";
        } else {
            // Returns the style-tag content when the menu is inactive
            return "visibility:hidden;display:none;";
        }
    }

    /**
     * Render method
     */
    render() {

        // Calculate the context menu options
        let options = [];

        // For every option of the menu
        for (let option of this.state.options) {

            options.push(
                <div className={styles['contextEntry']} onClick={(event) => {
                    option.onClick(event);
                    this.hide();
                }}>
                    {option.title}
                </div>
            );

        }

        return (
            <div style={this.getStyle()} className={styles['contextMenu']}>
                {options}
            </div>
        );
    }

}

export default ContextMenu;