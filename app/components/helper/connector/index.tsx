import { h, Component, cloneElement, } from 'preact';
import { IElement } from "../../elements/element";

export class Connector extends Component<any, {
	text: string
}> {
	isConnected: boolean;
	connector: Connector;
	parent: IElement;
	private static firstConnector: Connector;
	private static connectionCount = 0;
	constructor(props) {
		super(props);
		this.state.text = "";
		this.isConnected = false;
		this.parent = props.parent;
		this.connector = null;
		this.handleClickEvent = this.handleClickEvent.bind(this);
		if (props.ref) {
			props.ref(this);
		}
	}
	removeConnection() {
		if (this.isConnected) {
			this.state.text = "";
			this.isConnected = false;
			this.connector.removeConnection();
			this.forceUpdate();
			this.setState((o) => {
				o.connector = null;
			});
		}
	}
	handleClickEvent(e: Event) {
		e.preventDefault();
		if (Connector.firstConnector == null) {
			//First Clicked
			Connector.firstConnector = this;
			this.setState((s) => {
				s.text = Connector.connectionCount;
			});
		} else if (this.props.type !== Connector.firstConnector.props.type && this.parent !== Connector.firstConnector.parent) {
			this.isConnected = true;
			this.connector = Connector.firstConnector;
			this.setState((s) => {
				s.text = Connector.connectionCount++;
			});
			this.connector.isConnected = true;
			this.connector.connector = this;
			Connector.firstConnector = null;
		} else if (this === Connector.firstConnector) {
			Connector.firstConnector = null;
			this.setState((s) => {
				s.text = "";
			});
		}
	}

	render() {
		const p = <rect onClick={this.handleClickEvent}></ rect>;
		const text = <text onClick={this.handleClickEvent} text-anchor="middle" alignment-baseline="central" >{this.state.text}</text>;
		Object.assign(p.attributes, this.props);
		Object.assign(text.attributes, {
			x: this.props.x + this.props.width / 2,
			y: this.props.y + this.props.height / 2
		});
		return <g>{p}{text}</g>;
	}
}