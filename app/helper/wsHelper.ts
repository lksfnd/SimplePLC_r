enum WsState {
  NotConnected,
  Connected
}
enum AdsMemory {
  Master = 'Master',
  Slave = 'Slave',
  RetainData = 'RetainData'
}

interface IWsData {
  data: {};
  id: number;
}
interface IAmsConnInfo {
  ams_net_id: number[];
  ams_port: string;
}

export class WsHelper {
  private state: WsState;
  private ws: WebSocket;
  private queue: Map<number, (value?: IWsData | PromiseLike<IWsData>) => void>;
  constructor(url: string) {
    this.state = WsState.NotConnected;
    this.ws = new WebSocket(url);
    this.queue = new Map();
    this.ws.onmessage = (e: MessageEvent) => {
      const data: IWsData = JSON.parse(e.data);
      if (this.queue.has(data.id)) {
        const res = this.queue.get(data.id);
        res(data);
      } else {
        console.info(data);
      }
    };
  }
  private send(msg: IWsData) {
    this.ws.send(JSON.stringify(msg));
    let r: (value?: IWsData|PromiseLike<IWsData>) => void;
    const p = new Promise<IWsData>((res, rev) => {
      r = res;
    });
    this.queue.set(msg.id, r);
    return p;
  }
  listPlcs() {
    return this.send({id: Date.now(), data: {type: 'ListPlc'}});
  }
  connect(plc: IAmsConnInfo) {
    if (this.state === WsState.Connected) {
      throw new Error('cant connect to plc, still connected');
    } else {
      // TODO
      this.send({data: {type: 'Connect', ...plc}, id: Date.now()});
    }
  }
  disconnect() {
    if (this.state === WsState.NotConnected) {
      throw new Error('cant disconnect from plc, not connected');
    } else {
      // TODO
      return this.send({id: Date.now(), data: {type: 'Disconnect'}});
    }
  }
  read(memory: AdsMemory, length: number) {
    if (this.state === WsState.NotConnected) {
      throw new Error('cant read from plc, not connected');
    } else {
      // TODO
      return this.send({data: {type: 'Read', memory, length}, id: Date.now()});
    }
  }
  write(memory: AdsMemory, data: number[]) {
    if (this.state === WsState.NotConnected) {
      throw new Error('cant write from plc, not connected');
    } else {
      // TODO
      return this.send({data: {type: 'Write', memory, data}, id: Date.now()});
    }
  }
  listPrograms() {
    return this.send({id: Date.now(), data: {type: 'ListPrograms'}});
  }
  saveProgram(name: string, data: {}) {
    return this.send({id: Date.now(), data: {type: 'SaveProgram', data, name}});
  }
  loadProgram(name: string) {
    return this.send({id: Date.now(), data: {type: 'LoadProgram', name}});
  }
}