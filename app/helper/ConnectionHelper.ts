import { FunctionComponent, BoolComponent, IOComponent, MerkerComponent } from "../components/elements";
import { FunctionComponentType, Config } from "../config";
import { FunctionalComponent } from "preact";

interface ILogikParameter {
  stDataIn: Array<{
    bySourceType: number,
    byChannel: number
  }>,
  stDataout: Array<{
    bySourceType: number,
    byChannel: number
  }>
}
class ConnectionHelper {
  logicParameter: {
    stStandardLogicParameter: Array<FunctionComponent>,
    stComplexLogicParamter: Array<FunctionComponent>,
    stBuildingLogicParameter: Array<FunctionComponent>,
  };
  constructor() {
    this.logicParameter = {
      stBuildingLogicParameter: [],
      stStandardLogicParameter: [],
      stComplexLogicParamter: []
    }
  }
  addFunctionComponent(ele: FunctionComponent) {
    switch (ele.type) {
      case FunctionComponentType.STANDARD:
        this.logicParameter.stStandardLogicParameter.push(ele)
        break;
      case FunctionComponentType.COMPLEX:
        this.logicParameter.stComplexLogicParamter.push(ele)
        break;
      case FunctionComponentType.BUILDING:
        this.logicParameter.stBuildingLogicParameter.push(ele)
        break;
    }
  }
  static buildSingleConn(fc: FunctionComponent) {
    let inputConfig = fc.inputConnectors.map((i) => {
      if (i.isConnected) {
        let parent = i.connector.parent;
        return parent.getConnInfo(i.connector);
      } else {
        return {
          bySourceType: 0,
          byChannel: 0
        }
      }
    });
    let outputConfig = fc.outputConnectors.map((o) => {
      if (o.isConnected && !(o.connector.parent instanceof FunctionComponent)) {
        let parent = o.connector.parent;
        return parent.getConnInfo(o.connector);
      } else {
        return {
          bySourceType: 0,
          byChannel: 0
        }
      }
    });
    return [inputConfig, outputConfig];
  }
  buildConnections() {
    let s = this.logicParameter.stStandardLogicParameter.map(ConnectionHelper.buildSingleConn);
    let c = this.logicParameter.stComplexLogicParamter.map(ConnectionHelper.buildSingleConn);
    let b = this.logicParameter.stBuildingLogicParameter.map(ConnectionHelper.buildSingleConn);
    console.log(s, c, b);
  }
}

let conn = new ConnectionHelper();
export { conn as ConnectionHelper }