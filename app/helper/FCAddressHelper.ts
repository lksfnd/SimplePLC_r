import { FunctionComponent } from "../components/elements/index";
import { FunctionComponentType, Config } from "../config";

class FCAddressHelper {
	standard: Array<FunctionComponent> = [];
	complex: Array<FunctionComponent> = [];
	building: Array<FunctionComponent> = [];
	constructor() {
		this.standard.length = Config.fcAmount.STANDARD;
		this.complex.length = Config.fcAmount.COMPLEX;
		this.building.length = Config.fcAmount.BUILDING;
	}
	private getFreeElement(arr: Array<FunctionComponent>, ele: FunctionComponent): number {
		if (arr.indexOf(ele) > -1) {
			return arr.indexOf(ele);
		} else {
			for (let i = 0; i < arr.length; i++) {
				if (arr[i] == undefined) {
					arr[i] = ele;
					return i;
				}
			}
			return -1;
		}
	}
	private getArrayByType(type: FunctionComponentType) {
		if (type == FunctionComponentType.STANDARD) {
			return this.standard;
		} else if (type == FunctionComponentType.COMPLEX) {
			return this.complex;
		} else if (type == FunctionComponentType.BUILDING) {
			return this.building;
		}
	}
	public getAddress(ele: FunctionComponent) {
		let r = this.getFreeElement(this.getArrayByType(ele.type), ele);
		if (r < 0) {
			throw "not enough free addressess";
		}
		return r;
	}
	public freeAddress(ele: FunctionComponent) {
		this.getArrayByType(ele.type)[ele.addr] = undefined;
	}
}
let helper = new FCAddressHelper();
export { helper as FCAddressHelper }