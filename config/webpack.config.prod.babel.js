/* global __dirname */
import webpack from 'webpack';
import config from './webpack.config';
import OfflinePlugin from 'offline-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
let z = config({

    devtool: 'source-map',

    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),

        new UglifyJsPlugin(),

        new ExtractTextPlugin('[name].css'),

        new OfflinePlugin({
            AppCache: false,
            ServiceWorker: {
                events: true
            }
        })
    ]
}, true);

export default z