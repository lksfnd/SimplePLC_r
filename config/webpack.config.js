/* global __dirname */
import webpack from 'webpack';
import postcss, { getCSSLoaderConfig } from './postcss.config';
import path from 'path';
import packageJson from '../package.json';
const basePath = path.join(__dirname, '..', 'app');

export default ({
    plugins = [],
    resolve = {},
    devtool = 'eval',
}, prod = false) => {
    return {
        entry: {
            app: path.join(basePath, 'app.tsx'),
            vendor: Object.keys(packageJson.dependencies)
        },

        output: {
            path: path.join(basePath, '..', 'assets'),
            filename: '[name].js'
        },

        devtool,
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                filename: '[name].js'
            }),

            new webpack.LoaderOptionsPlugin({ options: { postcss } })

        ].concat(plugins),

        resolve: Object.assign({}, {

            modules: [
                'node_modules',
                'app'
            ],
            extensions: [".ts", ".tsx", ".js"]

        }, resolve),

        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: "ts-loader",
                },
                {
                    test: /\.svg$/,
                    loader: 'svg-inline-loader'
                },

                getCSSLoaderConfig(prod)
            ],
        }
    };
};
